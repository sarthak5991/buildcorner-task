$(document).ready( function() {
	$("#display-user").click( function(event) {
		event.preventDefault();

		if($("#left").css("display") != "none") {
			$("#left").hide().delay(30);
			$("#left-in-call").fadeIn(400);
		}
	});

	$("#call-icon").click( function(event) {
		event.preventDefault();

		if ($("#left-in-call").css("display") != "none") {
			$("#left-in-call").hide().delay(30);
			$("#during-call").fadeIn(400);

			$("#calling-table").transition('slide down');
		}
	});

	$("#end-call").click( function(event) {
		event.preventDefault();

		$("#during-call").hide().delay(30);
		$("#left").fadeIn(400);

		$("#calling-table").transition('slide up');
		
	});

	$(".contact-item").click(function(event) {
		console.log("clicked");
		event.preventDefault();

		if (!$(this).hasClass("active-contact")) {
			$(".active-contact").removeClass("active-contact");
			$(this).addClass("active-contact").animate(300);
		}
	})

});